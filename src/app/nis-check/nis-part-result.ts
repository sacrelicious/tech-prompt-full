export class NisPartResult {
  // The unique part identifier
  serial: string;

  // The starting date
  from: Date;

  // The ending date
  to: Date;

  // If there aren't any gaps between the from and to dates
  continuous: boolean;

  constructor() {
    this.serial = '';
    this.from = new Date();
    this.to = new Date();
    this.continuous = false;
  }
}

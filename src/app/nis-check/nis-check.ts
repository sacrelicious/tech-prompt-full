import { NisPartResult } from './nis-part-result';
import { NisDocument } from './nis-document';

export class NisCheck {
  public static process(documents: NisDocument[]): NisPartResult[] {


    const partA = new NisPartResult();
    partA.serial = 'A',
    partA.from = new Date('2012-01-01');
    partA.to = new Date('2012-02-01');
    partA.continuous = true;

    // TODO: return parts found in the documents instead of dummy data
    const parts = [partA];

    return parts;
  }
}
